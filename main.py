import time
import pycom
from pycoproc_1 import Pycoproc
import machine
from network import LoRa
import socket

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868)

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

from LIS2HH12 import LIS2HH12

pycom.heartbeat(False)
pycom.rgbled(0x0A0A08) # white

py = Pycoproc(Pycoproc.PYSENSE)

pybytes_enabled = True
if 'pybytes' in globals():
    if(pybytes.isconnected()):
        print('Pybytes is connected, sending signals to Pybytes')
        pybytes_enabled = True
li = LIS2HH12(py)

status = "normal"

if(pybytes_enabled):
    while True:
        if li.acceleration()[0] > 0.98: # Fenetre fermÃ©e
            pycom.rgbled(0x00ff00)
            if status == "normal":
                pybytes.send_signal(4, li.acceleration())
                status = "fermé"
                s.send('Fenetre fermée: ' + str(li.acceleration()))
                print("Fenetre fermee: " + str(li.acceleration()))
        else:
            pycom.rgbled(0xff0000)
            if status == "fermé":
                pybytes.send_signal(4, li.acceleration())
                status = "normal"
                s.send('Fenetre ouverte: ' + str(li.acceleration()))
                print("Fenetre ouverte: " + str(li.acceleration()))

        time.sleep(1)
